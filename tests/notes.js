process.env.NODE_ENV = 'testing';

const server = require('../app/server');
const MongoClient = require('mongodb').MongoClient;

const db = require('../config/db');

const whenConnected = MongoClient.connect(db.url);

const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
chai.should();

describe('Notes', () => {

  after(async () => {
    try {
      const db = await whenConnected;
      db.close();
    } catch (error) {
      console.log('Cannot close DB connections from tests!');
      throw error;
    }
  });

  // Clear the database before each test.
  beforeEach(done => {
    clearDatabase(whenConnected, done)
      .then(() => null);
  });

  describe('GET /notes', () => {
    it('Getting all notes list', done => {
      chai.request(server)
        .get('/notes')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array', "We are expecting an array of notes!");
          res.body.length.should.be.eql(0); // Because we have cleared db for each test.

          done();
        });
    });
  });

  describe('POST /notes', () => {

    // @todo: fail case

    it('Creating a note successful', done => {
      let note = {
        title: 'Testing',
        text: 'Testing creating a note from unit tests'
      };

      chai.request(server)
        .post('/notes')
        // .type('form') // If `bodyParser.urlencoded()`
        .send(note)
        .end((err, res) => {
          res.should.have.status(200);

          done();
        })
    })
  });

  describe('GET /notes/:id', () => {
    it('Getting the note by id', done => {
      createNote(whenConnected)
        .then(note => {
          chai.request(server)
            .get(`/notes/${note._id}`)
            .end((err, res) => {
              if (err) throw err;

              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('title');
              res.body.should.have.property('text');
              res.body.should.have.property('_id').eql(note._id.toString());

              done();
            })
        })
    })
  });

  describe('PUT /notes/:id', () => {

    // !! If send without fields you will get an empty object - {}
    it('Updating the note by id', done => {
      let expectedNote = {
        title: 'Update',
        text: 'text updated'
      };

      createNote(whenConnected)
        .then(note => {
          chai.request(server)
            .put(`/notes/${note._id}`)
            // .type('form') // If `bodyParser.urlencoded()`
            .send(expectedNote)
            .end((err, res) => {
              if (err) throw err;

              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('title').eql(expectedNote.title);
              res.body.should.have.property('text').eql(expectedNote.text);

              done();
            })
        })
    })
  });

  describe('DELETE /notes/:id', () => {
    it('Deleting the note by id', done => {
      createNote(whenConnected)
        .then(note => {
          chai.request(server)
            .delete(`/notes/${note._id}`)
            .end((err, res) => {
              res.should.have.status(200);
              res.should.be.a('object');
              // res.body.should.have.property('n').eql(1);
              // res.body.should.have.property('ok').eql(1);
              res.body.should.have.property('message').eql('Success');

              done();
            })
        })
    })
  });

});

// @todo: move it out of there to separate module
const deleteAll = (db, cb) => {
  db.collection('notes')
    .deleteMany({}, (err) => {
      if (err) {
        console.log('Cannot clear testing database!');
        throw err;
      }

      cb();
    });
};

const clearDatabase = async (whenConnected, done) => {
  try {
    const db = await whenConnected;
    deleteAll(db, done);
  } catch(error) {
    console.log('Cannot connect to testing database!', error);
    throw error;
  }
};

const createNote = async (whenConnected, note = {
  title: 'Default Title',
  text: 'default text'
}) => {
  try {
    const db = await whenConnected;

    return new Promise((resolve, reject) => {
      db.collection('notes').insertOne(note, (error, result) => {
        if (error) reject(err);

        resolve(result.ops[0]);
      })
    });
  // @todo: WTF? Fix it. Or remove try/catch.
  } catch(error) {
    throw error;
  }
};
