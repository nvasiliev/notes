### ToDo list

- [ ] Features
    - [ ] !! Pagination
    - [ ] !! Authentication
    - [ ] !! Finish validation
- [ ] Refactor
    - [x] Remove `app/utils/forceExit`
    - [ ] WIP Create `notes.routes.js` / `notes.model` / `notes.spec.js`
    - [ ] Improve error-handler middleware
        - [ ] `try/catch` with "layers" (see architecture notes)
        - [ ] Create custom errors
        - [ ] Develop best practices error handlers
        - [ ] Use different error response formats depends on request (JSON, HTML, etc.) - see the book
    - [ ] Architecture
        - [ ] !!! Add DI/IoC
        - [ ] !! Create `notes.repository` and `notes.service`.
        - [x] Remove callback hell from `server.js`, such as `app.list`, etc.  
- [ ] HTTP
    - [ ] Set JSON response headers
    - [ ] Valid HTTP status codes (now only 200 for non-error requests).
- [ ] CI / Infrastructure / Best Practices
    - [ ] Add docker
    - [ ] Try `llnode`!
    - [ ] Install `node-report` package
    - [ ] Install ESLint
    - [ ] Add `.sh` scripts folder.
    - [x] Install one of these: `dotdev`, `config` or `crossenv`
    - [ ] Try add the logger (see note 1 below)
- [ ] Profiling
    - [ ] Try use dev-tools chrome (memory snapshots, etc.)
- [ ] Testing
    - [x] Finish tests
    - [ ] Init test structure / Create `tests/setup.js`
    - [ ] Testing by spec files: `"test": "mocha ./**/*.spec.js"`    
- [ ] DB
    - [ ] Extract DB connection to separate module (maybe use EventEmitter)

- [x] Install mongoose`npm install mongoose`. See more:https://github.com/Automattic/mongoose

### Notes

#### 1 Logger notes:
```
var logFile = fs.createWriteStream('./myLogFile.log', {flags: 'a'}); // use {flags: 'w'} to open in write mode
app.use(express.logger({stream: logFile}));
```
See More:https://stackoverflow.com/a/9018195


#### cURL Request

>TIP: Open Postman, make request, then click to code (top-right corner). There you can present request in wide range of languages and tools

PUT

```
curl -X PUT -H "Content-Type: application/x-www-form-urlencoded" \
            -H "Cache-Control: no-cache" \
            -d 'title=Test__&text=Description' \
            "http://localhost:8000/notes/5a29c88b60f3702f8583fc00"
```
