const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  text: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  }
}, { timestamps: true });

/**
 * Middleware
 */
// schema.pre('save', (next) => ..)

/**
 * Methods
 */
// schema.methods.some

module.exports = mongoose.model('Note', schema);
