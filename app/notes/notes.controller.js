// @todo: !! Add repository & service layers.
const Note = require('./note.model');

// @todo: Maby create contoller-wrapper function to wrap try / catch,
// see `controllerHandler` fn here: https://github.com/nvasiliev/express-api-starter-kit/blob/master/src/routes/index.js

// @todo: Create try / catch wrapper.
// @todo: Move to `app/http`
class ValidationError extends Error {}
class HttpError extends Error {
  constructor(options) {
    super(options.message);
    this.status = options.status || 500;
  }
}
class HttpNotFoundError extends HttpError {
  constructor(message) {
    super({message, status: 404});
  }
}

class NoteController {

  /**
   * GET. Returns notes list.
   *
   * @param req
   * @param res
   * @param next
   * @returns {Promise.<void>}
   */
  async index(req, res, next) {
    // @todo: Add pagination by `limit` & `from` params.
    try {
      const notes = await Note.find({}).exec();
      res.send(notes);
    } catch (err) {
      next(new HttpError(`Can't get the notes`));
    }
  };


  /**
   * POST. Creates new note.
   *
   * @param req
   * @param res
   * @param next
   * @returns {Promise.<void>}
   */
  async create(req, res, next) {
    const params = {
      text: req.body.text,
      title: req.body.title
    };

    try {
      const note = new Note(params);
      await note.validate();

      const newNote = await note.save();
      res.send(newNote);
    } catch (validationError) {
      next(validationError);

      // @todo: Create `ValidationError`
      // Better way: throw ValidationError(validationError.message)
      // next(new ValidationError({
      //   status: 422,
      //   message: validationError.message
      // }));
    }
  };

  /**
   * GET. Returns note by ID.
   *
   * @param req
   * @param res
   * @param next
   * @returns {Promise.<void>}
   */
  async show(req, res, next) {
    try {
      const note = await Note.findOne({_id: req.params.id});

      // @todo: Raise `HttpNotFoundError`.
      res.send(note || { message: 'Not Found' });
    } catch (err) {
      next(err);
    }
  }

  async update(req, res, next) {
    try {
      const note = await Note.findOne({ _id: req.params.id });

      // @todo: lodash `only`
      Object.assign(note, req.body);
      await note.save();

      res.send(note);
    } catch (err) {
      next(err);
    }
  }

  async delete(req, res, next) {
    try {
      let message = 'Success';
      await Note.deleteOne({ _id: req.params.id }, err => message = 'Note not found');
      res.send({ message });
    } catch (err) {
      next(err);
    }
  }

}

module.exports = new NoteController();
