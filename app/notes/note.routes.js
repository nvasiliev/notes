const express = require('express');
const router = express.Router();

const notesController = require('./notes.controller');

router
  .get('/', notesController.index)
  .post('/', notesController.create)
  .get('/:id', notesController.show)
  .put('/:id', notesController.update)
  .delete('/:id', notesController.delete);

module.exports = router;
