const log = console.log;

class EnvironmentError extends Error {}

if (! process.env.NODE_ENV) {
  throw new EnvironmentError("Env is not loaded.")
}

const throwError = (message) => {
  log('error!', message);
  throw new Error(message);
};

const checkEnvironment = (envVariables) =>{
  if (! envVariables.length) {
    throwError('Env variables is empty');
    return process.exit(1);
  }

  envVariables.forEach(item => {
    if (! process.env[item]) {
      throwError(`Env variable is not loaded, variable: ${item}`);
      return process.exit(1);
    }
  });

};

module.exports = checkEnvironment;
