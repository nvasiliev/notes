require('dotenv').config();

/**
 * Checks for mandatory environment variables.
 */
require('./checkEnvironment')([
  'NODE_ENV',
  'APP_PORT',
  'DB_HOST',
  'DB_TEST_HOST'
]);
