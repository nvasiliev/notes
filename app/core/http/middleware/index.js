const bodyParser = require('body-parser');

module.exports = [

  /**
   * To get access to body of POST parameters of 'x-www-form-urlencoded' type.
   */
  // bodyParser.urlencoded({ extended: true }),
  bodyParser.json(),

  // (req, res, next) => (console.log('test'), next())
  // ..

];
