const express = require('express');
const router = express.Router();

router.get('/', (req, res) => res.json({ message: 'index' }));
router.use('/notes', require('./notes/note.routes'));

module.exports = router;
