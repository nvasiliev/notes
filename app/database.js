const mongoose = require('mongoose');
const log = console.log;

const config = require('../config');

/**
 * Fix for "DeprecationWarning: Mongoose: mpromise (mongoose's default promise library) is deprecated,
 * plug in your own promise library instead: http://mongoosejs.com/docs/promises.html"
 */
mongoose.Promise = Promise;

const connection = mongoose.connection;

/**
 * See: http://mongoosejs.com/docs/api.html#connection_Connection
 */
connection
  .on('connecting',     () => log('Connecting to DB..'))
  .on('connected',      () => log('DB connected successfully'))
  .on('disconnecting',  () => log('DB is disconnecting..'))
  .on('disconnected',   () => log('DB has disconnected'))
  .on('close',          () => log('DB connection has closed'))
  .on('reconnected',    () => log('DB has reconnected'))
  .on('error',          (error) => log('DB connection error!', error))
  .on('fullsetup',      () => log('DB connection replica-set scenario!'))
  .on('all',            () => log('DB connection all nodes are connected!'))
  .on('open',           () => log('DB connection is open successfully'));


/**
 * Expose connect function.
 */
const connect = () => {
  mongoose.connect(config.db.url, { useMongoClient: true })
    .catch(err => log('Can\'t connect to DB!'));

  return connection;
};

module.exports = connect;
