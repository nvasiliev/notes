const log = console.log;

// @todo: Maybe express provides another ways, search for it..
const errorHandler = (err, req, res, _next) => {
  // @todo: Create `ApiError` (and maybe other) & check for it here.

  log('--- Unexpected error ---');
  // log('Request:', req);
  log('Error:', err);

  return res.status(err.status || 500)
    .json({ error: err.message || 'Unexpected error' });
};

module.exports = errorHandler;
