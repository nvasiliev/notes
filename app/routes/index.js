const noteRoutes = require('./notes');

const log = console.log;

module.exports = (app, db) => {

  app.get('/', (req, res, next) => {
    res.json({message: 'index stub'});
  });

  app.get('/api*', (req, res) => {
    res.json({message: 'not found'});
  });

  // noteRoutes(app, db);

  app.get('/test-error', (req, res) => {
    log('Fire exception');
    // test = foo; // Uncomment to fire Unexpected error.
    throw new Error('Test Error-handler');
  });

  /**
   * Error-handler.
   */
  app.use(errorHandler);
};

// @todo: Maybe express provides another ways, search for it..
function errorHandler(err, req, res, _next) {
  // Expected errors always throw Error.
  // Unexpected errors will either throw unexpected stuff or crash the application.
  if (Object.prototype.isPrototypeOf.call(Error.prototype, err)) {
    return res.status(err.status || 500).json({ error: err.message });
  }

  log('--- !! Unexpected error !!  ---');
  log(req);
  log(err);

  return res.status(500).json({ error: 'Unexpected error' });
}
