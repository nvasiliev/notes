// const ObjectID = require('mongodb').ObjectID;

// module.exports = (app, db) => {

    // GET / (Index)
    // app.get('/notes', (req, res) => {
    //     // db.collection('notes').find({}, (err, notes) => {
    //     db.collection('notes').find({}).toArray((err, notes) => {
    //         if (err) {
    //             res.send({"error": "Can't get the notes"});
    //
    //             return;
    //         }
    //
    //         res.send(notes);
    //     });
    // });
    //
    // // POST /notes (Create)
    // app.post('/notes', (req, res) => {
    //     const note = {
    //         text: req.body.text,
    //         title: req.body.title
    //     };
    //
    //     db.collection('notes').insertOne(note, (err, result) => {
    //         if (err) {
    //             res.send({"error": "Can't insert new note"});
    //
    //             return;
    //         }
    //
    //         res.send(result.ops[0]);
    //     })
    // });

    // GET /notes/:id (Show)
    // app.get('/notes/:id', (req, res) => {
    //     const id = req.params.id;
    //     const node = {
    //         '_id': new ObjectID(id)
    //     };
    //
    //     db.collection('notes').findOne(node, (err, item) => {
    //        if (err) {
    //            res.send({'error': 'Error occurred'});
    //
    //            return;
    //        }
    //
    //        res.send(item);
    //     });
    // });
    //
    // // DELETE /notes/:id
    // app.delete('/notes/:id', (req, res) => {
    //     const id = req.params.id;
    //     const note = {
    //         '_id': new ObjectID(id)
    //     };
    //
    //     db.collection('notes').deleteOne(note, (err, item) => {
    //         if (err) {
    //             res.send({'error': 'Error occurred'});
    //
    //             return;
    //         }
    //
    //         res.send(item);
    //     })
    // });
    //
    // // PUT /notes/:id (Update)
    // app.put('/notes/:id', (req, res) => {
    //     const id = req.params.id;
    //     const searchNote = {
    //         '_id': new ObjectID(id)
    //     };
    //     const note = {
    //         text: req.body.text,
    //         title: req.body.title
    //     };
    //
    //     db.collection('notes').updateOne(searchNote, note, (err, result) => {
    //         if (err) {
    //             res.send({'error': 'Error occurred'});
    //
    //             return;
    //         }
    //
    //         res.send(note);
    //     });
    //
    // });
// };
