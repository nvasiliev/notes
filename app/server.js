require('./core/bootstrap/boot');

const express = require('express');
const app = express();

const config = require('../config');

// @todo: Change port for the Unit Tests!
const port = config.server.port;
// const port = 9999;

const env = config.server.env;

const connect = require('./database');
connect()
  .once('open', startServer);

app.use(require('./core/http/middleware'));
app.use('/', require('./routes'));

/**
 * Error-handler. Must be after all routes & middlewares.
 */
app.use(require('./handleError'));

function startServer() {
  app.listen(port);

  // @todo: Disable logger in testing env.
  console.log(`\nApp is running on ${port}\nEnvironment is: ${env}\n`);
}

/**
 * Needs to expose for testing.
 */
module.exports = app;
