const port = process.env.APP_PORT;
const env = process.env.NODE_ENV;
const db = require('./db');

module.exports = {
  db,
  server: {
    port,
    env
  }
};
