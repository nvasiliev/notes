const env = process.env.NODE_ENV;
const host = process.env.DB_HOST;
const testHost = process.env.DB_TEST_HOST;

const url = env === 'testing'
  ? testHost
  : host;

console.log('Database url:', url);

module.exports = {
    url
};
