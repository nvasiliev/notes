# Notes
This is simple minimal project written using Node.js.  
The project's goal is learn Node.js, best practices, etc.

The app represents a notes RESTful API server.

App supports only:

Method   | URI           | Description
---      | ---           | ---
`GET`    |  `/`          | Just stub for the root
`GET`    | `/notes`      | Getting all notes (pagination support 0in the fute) 
`POST`   | `/notes`      | Create new note, params: `title` and `text`
`GET`    | `/notes/{id}` | Create one note by `id`
`PUT`    | `/notes/{id}` | Update note
`DELETE` | `/note/{id}`  | Delete note

See [Task list](todo.md) with future plans, etc.

## Development

### Deploy (for local development)

Make `.env` file and fill out it:

```
cp .env.example .env
  
# File contains minimal comments  
vim .env
```

Install dependencies (see `package.json`)
```
npm install
```

Also, you have to install MongoDB server on local machine.  
[See bellow](#install-mongo-db-for-macos)

Otherwise, you can use side service.

### Run the server:

```
# Run MongoDB server:
mongod
  
# Run Node.js server
npm run start 
    
# Or run it with `nodemon` (e.g. hot reaload):
npm run hot   
```

### Run tests
```
npm run test
```

## MongoDB

#### Run Mongo DB Server

```
# Run
mongod
    
# If you do not use the default data directory (i.e., /data/db)
mongod --dbpath <path to data directory>
```

#### Stop

Mac OS
```
top | grep mongod # First columnd is PID.
kill -2 <PID>.
```

Windows:

```
# If installed as service:
net stop MongoDB
    
# If simple installation:
taskkill /f /im mongod.exe
```

See more `mongod --help`.

#### MongoDB client

>You must installed plugin: "Mongo Plugin".  
https://github.com/dboissier/mongo4idea

In PHPStorm:

`View -> Tool Windows -> MongoExplorer`.

#### Config

```
# Main config path.
vim /usr/local/etc/mongod.conf

# Log path (see in config above).
```

## Installation flow

This section describes installation flow as a guid to build project from zero.

>For project deploy you can just type `npm install`

@todo: Update flow (mongoose, etc.)

```
npm init
    
# Basic    
npm install --save express mongodb body-parser
    
# Optional
npm install --save-dev nodemon
    
# Testing
npm install mocha
npm install chai
npm install chai-http
```

If the `nodemon` has been installed add the following to the`package.json`
```
"scripts": {
    "hot": "nodemon server.js"
},
```

So now you can:

```
npm run hot
```

#### Install Mongo DB (for MacOS)
```
# devel - latest development release
brew install mongodb --with-openssl --devel
    
#  Create the directory to which the mongod process will write data.
mkdir -p /data/db
```
